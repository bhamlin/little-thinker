use error_stack::{Result, ResultExt};
#[allow(unused)]
use log::{info, Level};
use thinker::{error::MachineError, machine::Machine};

fn main() -> Result<(), MachineError> {
    // Manually init env_logger to debug
    env_logger::Builder::from_env(
        env_logger::Env::default().default_filter_or(Level::Debug.as_str()),
    )
    .init();

    // Load machine state from json "image"
    // Data in json is a selection of the Commodore 64's boot rom, and segments
    // created to allow the initialization to complete without actually
    // emulating more of the hardware of that system
    let mut machine = Machine::from_json("sample-image.json")?;

    loop {
        // Step and print machine state on error
        machine
            .step()
            .attach_printable_lazy(|| machine.to_string())?;
    }
}
