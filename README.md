# Little Thinker

A (bad) 6502 emulator in rust.

Using resouces located at:
* http://www.6502.org/users/obelisk/6502/reference.html
* https://www.c64-wiki.com/wiki/Reset_(Process)
* https://www.masswerk.at/6502/6502_instruction_set.html
* http://codebase64.org/doku.php?id=base:6502_6510_coding
* https://www.youtube.com/watch?v=fWqBmmPQP40
