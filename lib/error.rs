use error_stack::Context;
use std::fmt::Display;

#[derive(Debug)]
pub enum MachineError {
    UnknownOpCode,
    UnimplementedOpCode,
    FileIoError,
    BadHexError,
    ConfigParseError,
    SegmentTooLong,
    StackOverflow,
    StackUnderflow,
    OffTheEnd,
}

impl Context for MachineError {}
impl Display for MachineError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Self::UnknownOpCode => "Found unknown opcode",
            Self::UnimplementedOpCode => "Found unimplemented opcode",
            Self::FileIoError => "Error in file io operation",
            Self::BadHexError => "Bad value while decoding hex",
            Self::ConfigParseError => "Error reading configuration file",
            Self::SegmentTooLong => "Attempted to write out of bounds",
            Self::StackOverflow | Self::StackUnderflow => "Stack overflow/underflow",
            Self::OffTheEnd => "Access past end of memory",
        };

        write!(f, "{message}")
    }
}
