use crate::{
    error::MachineError,
    types::{Byte, Word},
};
use error_stack::{Report, Result};
use itertools::Itertools;
#[allow(unused)]
use log::{debug, trace};
use std::{collections::HashMap, fmt::Display};

/// RAM
pub struct Ram {
    pub data: [u8; 0x10000],
}

impl Display for Ram {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut last_line = [0; 64];
        let mut has_ellipsesed = false;
        for (row, chunk) in self.data.chunks(64).enumerate() {
            if row > 0 && row < 1023 {
                if chunk.eq(&last_line) {
                    if !has_ellipsesed {
                        writeln!(f, "      [...]")?;
                        has_ellipsesed = true;
                    }
                    continue;
                } else {
                    last_line.copy_from_slice(chunk);
                    has_ellipsesed = false;
                }
            }
            write!(f, "{:04x}: ", row * 64)?;
            for segment in chunk.chunks(16) {
                for byte in segment {
                    write!(f, "{byte:02x}")?;
                }
                write!(f, " ")?;
            }
            if row < 1023 {
                writeln!(f)?;
            }
        }
        std::fmt::Result::Ok(())
    }
}

impl Ram {
    /// Returns a ram chunk set to all zeroes.
    pub fn initialized() -> Self {
        Self { data: [0; 0x10000] }
    }

    /// Load ram from image.
    ///
    /// TODO: One day I'll support loading rom images of some description.
    /// Instead, see [`from_map()`](thinker::machine::memory::Ram::from_map) instead.
    pub fn from_image() -> Result<Self, MachineError> {
        todo!()
    }

    /// Load ram state from map.
    ///
    /// Expects chunks with the entry point as the key value.
    pub fn from_map(map: HashMap<Word, Vec<Byte>>) -> Result<Self, MachineError> {
        let mut ram = Ram::initialized();
        for (entry, data) in map {
            let entry = entry as usize;
            let len = data.len();

            trace!("Entry point: 0x{entry:04x}");
            let wrote = ram.memcpy_in(entry, len, &data)?;
            trace!("Wrote {wrote} bytes");
        }

        Ok(ram)
    }

    /// Save ram to image.
    ///
    /// TODO: One day I'll fill this in.
    pub fn to_image(&self) -> Result<(), MachineError> {
        todo!()
    }

    /// Get entire stack location as hex string. Not how I want to do this, but fine for now.
    pub fn get_stack(&self) -> String {
        self.data[0x0100..0x0200]
            .iter()
            .map(|v| format!("{v:02x}"))
            .collect_vec()
            .join("")
    }

    /// Clear ram contents.
    pub fn reset(&mut self) {
        self.data.fill(0x00);
    }

    /// `memcpy` for map loader.
    ///
    /// Accepts an entry point, a hint to the length of data, and a buffer containing values to copy.
    pub fn memcpy_in(
        &mut self,
        start: usize,
        length: usize,
        data: &[u8],
    ) -> Result<usize, MachineError> {
        let room = (0x10000 - start) as u16;

        if length > room as usize {
            let message = format!("Attempted to write {length} bytes in {room} bytes of space");
            let context = MachineError::SegmentTooLong;
            let report = Report::new(context);
            let report = report.attach_printable(message);
            return Err(report);
        }

        Ok(static_memcpy(&mut self.data[start..start + length], data))
    }

    /// Value at memory address.
    pub fn value_at(&self, address: Word) -> Byte {
        self.data[address as usize]
    }

    /// Value array starting at memory address.
    pub fn value_at_stream(&self, address: Word, count: usize) -> Vec<Byte> {
        self.data
            .iter()
            .skip(address as usize)
            .take(count)
            .cloned()
            .collect_vec()
    }
}

/// `memcpy` implementation for the arrays I'm using. There's probably a better way to do this.
pub fn static_memcpy(destination: &mut [u8], data: &[u8]) -> usize {
    let mut count = 0;
    for (dst, src) in destination.iter_mut().zip(data) {
        *dst = *src;
        count += 1;
    }
    count
}
