use crate::machine::cpu::flags::*;
use crate::types::{Bit, Byte, Word};
use std::{
    fmt::{Debug, Display},
    ops::BitAnd,
};

/// Flag Register trait because I am still having a hard time leaving OO.
///
/// Whether or not this was a good choice, I made it for the wrong reasons.
pub trait FlagRegister<Size>: Display
where
    Size: Copy,
{
    fn flag_get(&self, mask: Size) -> Bit;
    fn flag_set(&mut self, mask: Size);
    fn flag_clear(&mut self, mask: Size);
}

#[derive(Default)]
/// Program counter register
pub struct ProgramCounter {
    pub value: Word,
}

impl Display for ProgramCounter {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let value = self.value;
        write!(f, "0x{value:04x} ({value:5})")
    }
}

impl ProgramCounter {
    /// Advance program counter by one, and return old location.
    pub fn advance(&mut self) -> Word {
        let old = self.value;
        self.value += 1;
        old
    }
}

#[derive(Default)]
pub struct ByteRegister {
    pub value: Byte,
}

impl Display for ByteRegister {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let value = self.value;
        write!(f, "0x{value:02x} ({value:3})")
    }
}

impl ByteRegister {
    pub fn increment(&mut self) -> Byte {
        self.add(1)
    }

    pub fn decrement(&mut self) -> Byte {
        self.subtract(1)
    }

    pub fn add(&mut self, value: Byte) -> Byte {
        let old_value = self.value;
        let v = self.value as u16;
        let v = v + value as u16;
        if v > 0xff {
            // Overflowed
            self.value = (v & 0xff) as u8;
        } else {
            self.value = v as u8;
        }
        old_value
    }

    pub fn subtract(&mut self, value: Byte) -> Byte {
        let old_value = self.value;
        if value > self.value {
            // Underflowed
            let mut temp = self.value as u16;
            temp += 0x0100;
            temp -= value as u16;
            self.value = temp as u8;
        } else {
            self.value -= value;
        }
        old_value
    }
}

#[derive(Default)]
/// Status Register specifically for flag storage operations
pub struct StatusRegister {
    pub value: Byte,
}

impl Debug for StatusRegister {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self, f)
    }
}

impl Display for StatusRegister {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let flags = format!(
            "{}{}_{}{}{}{}{}",
            Self::flag_status(self.value, STATUS_FLAG_NEGATIVE, "N", "_"),
            Self::flag_status(self.value, STATUS_FLAG_OVERFLOW, "V", "_"),
            Self::flag_status(self.value, STATUS_FLAG_BREAK, "B", "_"),
            Self::flag_status(self.value, STATUS_FLAG_DECIMAL_MODE, "D", "_"),
            Self::flag_status(self.value, STATUS_FLAG_IRQ_DISABLE, "I", "_"),
            Self::flag_status(self.value, STATUS_FLAG_ZERO, "Z", "_"),
            Self::flag_status(self.value, STATUS_FLAG_CARRY, "C", "_"),
        );
        write!(f, "0x{:02x} [{flags}]", self.value)
    }
}

impl FlagRegister<Byte> for StatusRegister {
    fn flag_get(&self, mask: Byte) -> Bit {
        mask == (self.value & mask)
    }

    fn flag_set(&mut self, mask: Byte) {
        self.value |= mask;
    }

    fn flag_clear(&mut self, mask: Byte) {
        self.value &= !mask;
    }
}

impl StatusRegister {
    fn flag_status<Size, T>(value: Size, mask: Size, is_set: T, not_set: T) -> T
    where
        Size: BitAnd<Output = Size> + Copy + PartialEq,
    {
        if mask == (value & mask) {
            is_set
        } else {
            not_set
        }
    }
}
