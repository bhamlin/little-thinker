use crate::types::Byte;

/// Carry Flag
/// -------
/// The carry flag is set if the last operation caused an overflow from bit 7
/// of the result or an underflow from bit 0. This condition is set during
/// arithmetic, comparison and during logical shifts. It can be explicitly set
/// using the 'Set Carry Flag' (SEC) instruction and cleared with 'Clear Carry
/// Flag' (CLC).
pub const STATUS_FLAG_CARRY: Byte = 0x01;

/// Zero Flag
/// -------
/// The zero flag is set if the result of the last operation as was zero.
/// * 0 = Result not zero
/// * 1 = Rezult is zero
pub const STATUS_FLAG_ZERO: Byte = 0x02;

/// IRQ Disable
/// -------
/// The interrupt disable flag is set if the program has executed a 'Set
/// Interrupt Disable' (SEI) instruction. While this flag is set the processor
/// will not respond to interrupts from devices until it is cleared by a 'Clear
/// Interrupt Disable' (CLI) instruction.
/// * 0 = IRQ Enabled?
/// * 1 = IRQ Disabled?
pub const STATUS_FLAG_IRQ_DISABLE: Byte = 0x04;

/// Decimal Mode
/// -------
/// While the decimal mode flag is set the processor will obey the rules of
/// Binary Coded Decimal (BCD) arithmetic during addition and subtraction. The
/// flag can be explicitly set using 'Set Decimal Flag' (SED) and cleared with
/// 'Clear Decimal Flag' (CLD).
pub const STATUS_FLAG_DECIMAL_MODE: Byte = 0x08;

/// Break Command
/// -------
/// The break command bit is set when a BRK instruction has been executed and
/// an interrupt has been generated to process it.
pub const STATUS_FLAG_BREAK: Byte = 0x10;

/// Unused
/// -------
// pub const STATUS_FLAG_UNUSED: Byte = 0x20;

/// Overflow Flag
/// -------
/// The overflow flag is set during arithmetic operations if the result has
/// yielded an invalid 2's complement result (e.g. adding to positive numbers
/// and ending up with a negative result: 64 + 64 => -128). It is determined by
/// looking at the carry between bits 6 and 7 and between bit 7 and the carry
/// flag.
pub const STATUS_FLAG_OVERFLOW: Byte = 0x40;

/// Negative Flag
/// -------
/// The negative flag is set if the result of the last operation had bit 7 set
/// to a one.
pub const STATUS_FLAG_NEGATIVE: Byte = 0x80;
