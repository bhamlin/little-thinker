use crate::{
    error::MachineError,
    machine::{
        cpu::{
            flags::{STATUS_FLAG_DECIMAL_MODE, STATUS_FLAG_IRQ_DISABLE},
            instructions::{opcodes::Opcode, Instruction},
            registers::FlagRegister,
        },
        Machine,
    },
};
use error_stack::{Report, Result};
use log::debug;

pub fn execute_instruction(
    machine: &mut Machine,
    instruction: &Instruction,
) -> Result<(), MachineError> {
    match &instruction.opcode {
        Opcode::ClearDecimalMode => clear_decimal_mode(machine),
        Opcode::ClearInterruptDisable => clear_interrupt_disable(machine),
        Opcode::SetInterruptDisable => set_interrupt_disable(machine),

        operation => Err(Report::new(MachineError::UnimplementedOpCode)
            .attach_printable(format!("Not implemented yet: {operation:?}"))),
    }
}

/// CLD - Clear Decimal Mode
/// ---
/// Sets the decimal mode flag to zero.
fn clear_decimal_mode(machine: &mut Machine) -> Result<(), MachineError> {
    debug!("CLD");
    machine.cpu.status.flag_clear(STATUS_FLAG_DECIMAL_MODE);

    Ok(())
}

/// CLI - Clear Interrupt Disable
/// ---
/// Clears the interrupt disable flag allowing normal interrupt requests to be serviced.
fn clear_interrupt_disable(machine: &mut Machine) -> Result<(), MachineError> {
    debug!("CLI");
    machine.cpu.status.flag_clear(STATUS_FLAG_IRQ_DISABLE);

    Ok(())
}

/// SEI - Set Interrupt Disable
/// ---
/// Set the interrupt disable flag to one.
fn set_interrupt_disable(machine: &mut Machine) -> Result<(), MachineError> {
    debug!("SEI");
    machine.cpu.status.flag_set(STATUS_FLAG_IRQ_DISABLE);

    Ok(())
}
