use crate::{
    error::MachineError,
    machine::{
        cpu::{
            execution::{set_carry, set_negative, set_zero},
            instructions::{opcodes::Opcode, Instruction},
        },
        Machine,
    },
};
use error_stack::{Report, Result};
use log::debug;

pub fn execute_instruction(
    machine: &mut Machine,
    instruction: &Instruction,
) -> Result<(), MachineError> {
    match &instruction.opcode {
        Opcode::CompareAbsoluteX => compare_absolute_x(machine),

        operation => Err(Report::new(MachineError::UnimplementedOpCode)
            .attach_printable(format!("Not implemented yet: {operation:?}"))),
    }
}

/// CMP - Compare (Absolute,X)
/// ---
/// This instruction compares the contents of the accumulator with another memory held value and sets the zero and carry flags as appropriate.
fn compare_absolute_x(machine: &mut Machine) -> Result<(), MachineError> {
    let address = machine.next_address()?;
    let x = machine.cpu.reg_x.value as u16;
    let test_address = address + x;
    let m = machine.ram.value_at(test_address);
    let a = machine.cpu.reg_a.value;
    debug!("CMP ${address:04x},X (X: #${x:02x}) [P:${test_address:04x} A:#${a:02x} M:#${m:02x}]");

    set_carry(machine, a >= m);
    set_zero(machine, a == m);
    set_negative(machine, a >= m);

    Ok(())
}
