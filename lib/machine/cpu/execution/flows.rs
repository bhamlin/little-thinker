use crate::{
    error::MachineError,
    machine::{
        cpu::{
            flags::STATUS_FLAG_ZERO,
            instructions::{opcodes::Opcode, Instruction},
            registers::FlagRegister,
        },
        Machine,
    },
};
use error_stack::{Report, Result};
use log::debug;

pub fn execute_instruction(
    machine: &mut Machine,
    instruction: &Instruction,
) -> Result<(), MachineError> {
    match &instruction.opcode {
        Opcode::BranchIfNotEqual => branch_not_equal(machine),
        Opcode::JumpAbsolute => jump_absolute(machine),
        Opcode::JumpIndirect => jump_indirect(machine),
        Opcode::JumpSubRoutine => jump_to_subroutine(machine),
        Opcode::ReturnFromSubroutine => return_from_subroutine(machine),

        operation => Err(Report::new(MachineError::UnimplementedOpCode)
            .attach_printable(format!("Not implemented yet: {operation:?}"))),
    }
}

/// BNE - Branch if Not Equal
/// ---
/// If the zero flag is clear then add the relative displacement to the program counter to cause a branch to a new location.
fn branch_not_equal(machine: &mut Machine) -> Result<(), MachineError> {
    let current_address = machine.current_address;
    // TODO: Handle negative displacement one day...
    // TODO: Today will be that day it seems
    let displacement = machine.next_byte()? as u16;
    let address = machine.cpu.program_counter.value + displacement;
    debug!("{current_address:04x}: BNE ${address:02x}       [D:#${displacement:02x}]");
    if !machine.cpu.status.flag_get(STATUS_FLAG_ZERO) {
        machine.cpu.program_counter.value = address;
    }

    Ok(())
}

/// JMP - Jump (Absolute)
/// ---
/// Sets the program counter to the address specified by the operand.
fn jump_absolute(machine: &mut Machine) -> Result<(), MachineError> {
    let current_address = machine.current_address;
    let target_address = machine.next_address()?;
    debug!("{current_address:04x}: JMP ${target_address:04x}");
    machine.cpu.program_counter.value = target_address;

    Ok(())
}

/// JMP - Jump (Indirect)
/// ---
/// Sets the program counter to the address specified by value at the location specified by the operand.
fn jump_indirect(machine: &mut Machine) -> Result<(), MachineError> {
    let current_address = machine.current_address;
    let load_address = machine.next_address()?;
    let target_address = Machine::bytes_to_address(&machine.ram.value_at_stream(load_address, 2));
    debug!("{current_address:04x}: JMP (${load_address:04x}) -> {target_address:04x}");
    machine.cpu.program_counter.value = target_address;

    Ok(())
}

/// JSR - Jump to Subroutine
/// ---
/// The JSR instruction pushes the address of the return point on to the stack and then sets the program counter to the target memory address.
fn jump_to_subroutine(machine: &mut Machine) -> Result<(), MachineError> {
    let current_address = machine.current_address;
    let address = machine.next_address()?;
    debug!("{current_address:04x}: JSR ${address:04x}");
    machine.jump_to_sub(address)?;

    Ok(())
}

/// RTS - Return from Subroutine
/// ---
/// The RTS instruction is used at the end of a subroutine to return to the calling routine. It pulls the program counter from the stack.
fn return_from_subroutine(machine: &mut Machine) -> Result<(), MachineError> {
    let current_address = machine.current_address;
    debug!("{current_address:04x}: RTS");
    machine.return_from_sub()?;

    Ok(())
}
