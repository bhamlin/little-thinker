use crate::{
    error::MachineError,
    machine::{
        cpu::instructions::{opcodes::Opcode, Instruction},
        Machine,
    },
};
use error_stack::{Report, Result};
use log::debug;

pub fn execute_instruction(
    machine: &mut Machine,
    instruction: &Instruction,
) -> Result<(), MachineError> {
    match &instruction.opcode {
        Opcode::TransferXStack => transfer_x_to_stack(machine),

        operation => Err(Report::new(MachineError::UnimplementedOpCode)
            .attach_printable(format!("Not implemented yet: {operation:?}"))),
    }
}

/// TXS - Transfer X to Stack Pointer
/// ---
/// Copies the current contents of the X register into the stack register.
fn transfer_x_to_stack(machine: &mut Machine) -> Result<(), MachineError> {
    debug!("TXS");
    machine.cpu.stack_pointer.value = machine.cpu.reg_x.value;

    Ok(())
}
