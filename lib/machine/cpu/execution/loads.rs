use crate::{
    error::MachineError,
    machine::{
        cpu::{
            execution::{set_negative, set_zero},
            instructions::{opcodes::Opcode, Instruction},
        },
        Machine,
    },
};
use error_stack::{Report, Result};
use log::debug;

pub fn execute_instruction(
    machine: &mut Machine,
    instruction: &Instruction,
) -> Result<(), MachineError> {
    match &instruction.opcode {
        Opcode::LdaImmediate => load_a_immediate(machine),
        Opcode::LdaAbsoluteX => load_a_absolute_x(machine),
        Opcode::LdxImmediate => load_x_immediate(machine),

        operation => Err(Report::new(MachineError::UnimplementedOpCode)
            .attach_printable(format!("Not implemented yet: {operation:?}"))),
    }
}

/// LDA - Load Accumulator (Immediate)
/// ---
/// Loads a byte of memory into the accumulator setting the zero and negative flags as appropriate.
fn load_a_immediate(machine: &mut Machine) -> Result<(), MachineError> {
    let value = machine.next_byte()?;
    debug!("LDA #${value:02x}");
    machine.cpu.reg_a.value = value;

    set_zero(machine, value == 0);
    set_negative(machine, (value & 0b10000000) != 0);

    Ok(())
}

/// LDA - Load Accumulator (Absolute,X)
/// ---
/// Loads a byte of memory from the indicated location (modified by the X register) into the
/// accumulator setting the zero and negative flags as appropriate.
fn load_a_absolute_x(machine: &mut Machine) -> Result<(), MachineError> {
    let current_address = machine.current_address;
    let address = machine.next_address()?;
    let x = machine.cpu.reg_x.value;
    let load_address = address as usize + x as usize;
    let value = machine.ram.data[load_address];
    debug!("{current_address:04x}: LDA ${address:04x},X (X: #${x:02x})");
    machine.cpu.reg_a.value = value;

    set_zero(machine, value == 0);
    set_negative(machine, (value & 0b10000000) != 0);

    Ok(())
}

/// LDX - Load X Register (Immediate)
/// ---
/// Loads a byte of memory into the X register setting the zero and negative flags as appropriate.
fn load_x_immediate(machine: &mut Machine) -> Result<(), MachineError> {
    let current_address = machine.current_address;
    let value = machine.next_byte()?;
    debug!("{current_address:04x}: LDX #${value:02x}");
    machine.cpu.reg_x.value = value;

    set_zero(machine, value == 0);
    set_negative(machine, (value & 0b10000000) != 0);

    Ok(())
}
