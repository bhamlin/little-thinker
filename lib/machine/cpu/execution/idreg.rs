use crate::{
    error::MachineError,
    machine::{
        cpu::{
            execution::{set_negative, set_zero},
            instructions::{opcodes::Opcode, Instruction},
        },
        Machine,
    },
};
use error_stack::{Report, Result};
use log::debug;

pub fn execute_instruction(
    machine: &mut Machine,
    instruction: &Instruction,
) -> Result<(), MachineError> {
    match &instruction.opcode {
        Opcode::DecrementX => decrement_x(machine),

        operation => Err(Report::new(MachineError::UnimplementedOpCode)
            .attach_printable(format!("Not implemented yet: {operation:?}"))),
    }
}

/// DEX - Decrement X Register
/// ---
/// Subtracts one from the X register setting the zero and negative flags as appropriate.
fn decrement_x(machine: &mut Machine) -> Result<(), MachineError> {
    let current_address = machine.current_address;
    debug!("{current_address:04x}: DEX");
    let x = machine.cpu.reg_x.value;

    set_zero(machine, x == 0);
    set_negative(machine, 0 < (x & 0b10000000));

    Ok(())
}
