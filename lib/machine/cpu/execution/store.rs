use crate::{
    error::MachineError,
    machine::{
        cpu::instructions::{opcodes::Opcode, Instruction},
        Machine,
    },
};
use error_stack::{Report, Result};
use log::debug;

pub fn execute_instruction(
    machine: &mut Machine,
    instruction: &Instruction,
) -> Result<(), MachineError> {
    match &instruction.opcode {
        Opcode::StoreXAbsolute => store_x_absolute(machine),

        operation => Err(Report::new(MachineError::UnimplementedOpCode)
            .attach_printable(format!("Not implemented yet: {operation:?}"))),
    }
}

/// STX - Store X Register (Absolute)
/// ---
/// Stores the contents of the X register into memory.
fn store_x_absolute(machine: &mut Machine) -> Result<(), MachineError> {
    let address = machine.next_address()? as usize;
    let x = machine.cpu.reg_x.value;
    debug!("STX ${address:04x}   (X: #${x:02x})");
    machine.ram.data[address] = x;

    Ok(())
}
