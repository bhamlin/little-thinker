pub mod flags;
pub mod flows;
pub mod idreg;
pub mod loads;
pub mod stack;
pub mod store;
pub mod tests;

use super::{
    flags::{STATUS_FLAG_CARRY, STATUS_FLAG_NEGATIVE, STATUS_FLAG_ZERO},
    instructions::{Instruction, InstructionCategory},
    registers::FlagRegister,
};
use crate::{error::MachineError, machine::Machine};
use error_stack::{Report, Result};

/// Execute instruction
/// ---
/// Dispatches instruction to the appropriate handler module for logic execution
pub fn execute(machine: &mut Machine, instruction: &Instruction) -> Result<(), MachineError> {
    match instruction.opcode.get_category() {
        InstructionCategory::Flags => flags::execute_instruction(machine, instruction),
        InstructionCategory::Flows => flows::execute_instruction(machine, instruction),
        InstructionCategory::IDReg => idreg::execute_instruction(machine, instruction),
        InstructionCategory::Loads => loads::execute_instruction(machine, instruction),
        InstructionCategory::Stack => stack::execute_instruction(machine, instruction),
        InstructionCategory::Store => store::execute_instruction(machine, instruction),
        InstructionCategory::Tests => tests::execute_instruction(machine, instruction),

        category => Err(Report::new(MachineError::UnimplementedOpCode)
            .attach_printable(format!("Missing category: {category:?}"))),
    }
}

fn set_zero(machine: &mut Machine, state: bool) {
    set_flag(machine, STATUS_FLAG_ZERO, state);
}

fn set_negative(machine: &mut Machine, state: bool) {
    set_flag(machine, STATUS_FLAG_NEGATIVE, state);
}

/// Set carry helper
fn set_carry(machine: &mut Machine, state: bool) {
    set_flag(machine, STATUS_FLAG_CARRY, state);
}

/// Set carry helper
fn set_flag(machine: &mut Machine, mask: u8, state: bool) {
    match state {
        true => machine.cpu.status.flag_set(mask),
        false => machine.cpu.status.flag_clear(mask),
    };
}
