pub mod decode;
pub mod opcodes;

use self::opcodes::Opcode;
use crate::{error::MachineError, types::Byte};
use error_stack::Result;

#[derive(Debug)]
/// Opcode argument length. 0, 1, or 2 additional bytes.
pub enum OpArgs {
    None,
    Byte,
    Address,
}

// impl From<usize> for OpArgs {
//     fn from(value: usize) -> Self {
//         match value {
//             2 => Self::Address,
//             1 => Self::Byte,
//             0 => Self::None,
//             _ => Self::None,
//         }
//     }
// }

#[derive(Debug)]
/// Opcode category for execution module
pub enum InstructionCategory {
    None,
    Flags,
    Flows,
    IDReg,
    Loads,
    Stack,
    Store,
    Tests,
}

#[derive(Debug)]
/// Instruction
/// ---
/// Contains opcode, argument length, expected cycles, and whether or not this instruction can cause a paging delay
pub struct Instruction {
    pub opcode: Opcode,
    pub additional_bytes: OpArgs,
    pub cycle_advance: usize,
    pub page_delay: u8,
}

impl Instruction {
    /// New instruction with default 0 for page delay
    fn new(opc: Opcode, opa: OpArgs, ca: usize) -> Option<Self> {
        Self::new_delay(opc, opa, ca, 0)
    }

    /// New instruction with page delay
    fn new_delay(opc: Opcode, opa: OpArgs, ca: usize, d: u8) -> Option<Self> {
        Some(Instruction {
            opcode: opc,
            additional_bytes: opa,
            cycle_advance: ca,
            page_delay: d,
        })
    }

    /// Helper function to decode byte to instruction
    pub fn for_byte(value: Byte) -> Result<Self, MachineError> {
        decode::for_byte(value)
    }
}
