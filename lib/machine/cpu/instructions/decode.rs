use super::{opcodes::Opcode, Instruction, OpArgs};
use crate::{error::MachineError, types::Byte};
use error_stack::{Report, Result};

/// Decodes byte to opcode for execution
pub fn for_byte(value: Byte) -> Result<Instruction, MachineError> {
    let op = match value {
        0x20 => Instruction::new(Opcode::JumpSubRoutine, OpArgs::Address, 6),
        0x4c => Instruction::new(Opcode::JumpAbsolute, OpArgs::Address, 3),
        0x58 => Instruction::new(Opcode::ClearInterruptDisable, OpArgs::None, 2),
        0x60 => Instruction::new(Opcode::ReturnFromSubroutine, OpArgs::None, 6),
        0x6c => Instruction::new(Opcode::JumpIndirect, OpArgs::Address, 5),
        0x78 => Instruction::new(Opcode::SetInterruptDisable, OpArgs::None, 2),
        0x8e => Instruction::new(Opcode::StoreXAbsolute, OpArgs::Address, 4),
        0x9a => Instruction::new(Opcode::TransferXStack, OpArgs::None, 2),
        // 0xa1 => Instruction::new(Opcode::LdaIndirectX, OpArgs::Byte, 6),
        0xa2 => Instruction::new(Opcode::LdxImmediate, OpArgs::Byte, 2),
        // 0xa5 => Instruction::new(Opcode::LdaZeroPage, OpArgs::Byte, 3),
        0xa9 => Instruction::new(Opcode::LdaImmediate, OpArgs::Byte, 2),
        // 0xad => Instruction::new(Opcode::LdaAbsolute, OpArgs::Address, 4),
        // 0xb1 => Instruction::new_delay(Opcode::LdaIndirectY, OpArgs::Byte, 5, 1),
        // 0xb5 => Instruction::new(Opcode::LdaZeroPageX, OpArgs::Byte, 4),
        // 0xb9 => Instruction::new_delay(Opcode::LdaAbsoluteY, OpArgs::Address, 4, 1),
        0xbd => Instruction::new_delay(Opcode::LdaAbsoluteX, OpArgs::Address, 4, 1),
        0xca => Instruction::new(Opcode::DecrementX, OpArgs::None, 2),
        0xd0 => Instruction::new_delay(Opcode::BranchIfNotEqual, OpArgs::Byte, 2, 2),
        0xd8 => Instruction::new(Opcode::ClearDecimalMode, OpArgs::None, 2),
        0xdd => Instruction::new_delay(Opcode::CompareAbsoluteX, OpArgs::Address, 4, 1),
        // 0xf8 => Instruction::new(Opcode::SetDecimalMode, OpArgs::None, 2),
        _ => None,
    };

    op.ok_or(
        Report::new(MachineError::UnknownOpCode)
            .attach_printable(format!("Found unknown opcode: 0x{value:02x}")),
    )
}
