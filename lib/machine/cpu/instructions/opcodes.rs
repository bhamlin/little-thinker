use super::InstructionCategory;

#[derive(Debug)]
/// Opcode List
/// ---
/// Friendly names for opcodes
pub enum Opcode {
    // ADC,
    // AND,
    // ASL,
    // BCC,
    // BCS,
    // BEQ,
    // BIT,
    // BMI,
    BranchIfNotEqual,
    // BPL,
    // BRK,
    // BVC,
    // BVS,
    // CLC,
    ClearDecimalMode,
    ClearInterruptDisable,
    // CLV,
    // CMP,
    CompareAbsoluteX,
    // CPX,
    // CPY,
    // DEC,
    DecrementX,
    // DEY,
    // EOR,
    // INC,
    // INX,
    // INY,
    JumpAbsolute,
    JumpIndirect,
    JumpSubRoutine,
    LdaImmediate,
    // LdaZeroPage,
    // LdaZeroPageX,
    // LdaAbsolute,
    LdaAbsoluteX,
    // LdaAbsoluteY,
    // LdaIndirectX,
    // LdaIndirectY,
    LdxImmediate,
    // LDY,
    // LSR,
    NOP,
    // ORA,
    // PHA,
    // PHP,
    // PLA,
    // PLP,
    // ROL,
    // ROR,
    // RTI,
    ReturnFromSubroutine,
    // SBC,
    // SEC,
    SetDecimalMode,
    SetInterruptDisable,
    // STA,
    StoreXZeroPage,
    StoreXZeroPageY,
    StoreXAbsolute,
    StoreXAbsoluteX,
    StoreXAbsoluteY,
    // STY,
    // TAX,
    // TAY,
    // TSX,
    // TXA,
    TransferXStack,
    // TYA,
}

impl Opcode {
    /// Get Opcode Category
    /// ---
    /// Used to indicate which rust module opcode execution is defined in
    pub fn get_category(&self) -> InstructionCategory {
        match self {
            Self::CompareAbsoluteX => InstructionCategory::Tests,
            Self::LdaImmediate | Self::LdaAbsoluteX => InstructionCategory::Loads,
            Self::LdxImmediate => InstructionCategory::Loads,
            Self::StoreXZeroPage
            | Self::StoreXZeroPageY
            | Self::StoreXAbsolute
            | Self::StoreXAbsoluteX
            | Self::StoreXAbsoluteY => InstructionCategory::Store,
            Self::ClearDecimalMode
            | Self::ClearInterruptDisable
            | Self::SetDecimalMode
            | Self::SetInterruptDisable => InstructionCategory::Flags,
            Self::TransferXStack => InstructionCategory::Stack,
            Self::BranchIfNotEqual
            | Self::JumpAbsolute
            | Self::JumpIndirect
            | Self::JumpSubRoutine
            | Self::ReturnFromSubroutine => InstructionCategory::Flows,
            Self::DecrementX => InstructionCategory::IDReg,
            _ => InstructionCategory::None,
        }
    }
}
