use self::registers::{ByteRegister, ProgramCounter, StatusRegister};
use std::fmt::Display;

pub mod execution;
pub mod flags;
pub mod instructions;
pub mod registers;

#[derive(Default)]
/// 6502 CPU
/// ---
/// Basic relevant parts of a 6502 cpu for this project
pub struct Cpu {
    /// Not fully operational yet, but intends to count cycles to potentially detect accuracy
    pub cycle_counter: usize,

    // Pointers
    /// Program Counter to point at current memory location for operation
    pub program_counter: ProgramCounter,
    /// Stack Counter to point at current stack location
    pub stack_pointer: ByteRegister,

    // Registers
    pub reg_a: ByteRegister,
    pub reg_x: ByteRegister,
    pub reg_y: ByteRegister,

    // Flag bytes
    pub status: StatusRegister,
}

impl Display for Cpu {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{{ PC: {}, SP: {}, A: {}, X: {}, Y: {}, Status: {} }}",
            self.program_counter,
            self.stack_pointer,
            self.reg_a,
            self.reg_x,
            self.reg_y,
            self.status
        )
    }
}

impl Cpu {
    /// Returns a CPU model with a sensible(?) state.
    pub fn initialized() -> Self {
        let mut cpu = Self::default();
        cpu.reset();

        cpu
    }

    /// Attempts to build a sensible(?) state.
    ///
    /// An actual 6502 has some operations it performs when powered on. I am attempting
    /// here to apply that state to the model instead of actually performing those
    /// operations. If I can find opcodes for the operations performed, I might implement
    /// that here instead.
    ///
    /// As it is, I haven't looked to see if such a thing exists. Most likely this is not
    /// performed with actual opcodes but internal logic firing intentionally and
    /// accidentally within the 6502.
    pub fn reset(&mut self) {
        self.cycle_counter = 7;
        self.reg_a.value = 0;
        self.reg_x.value = 0;
        self.reg_y.value = 0;
        self.status.value = 0;
        self.stack_pointer.value = 0xfd;
        self.program_counter.value = 0xfffc;
    }

    pub fn advance_clock(&mut self, cycles: usize) {
        self.cycle_counter += cycles;
    }
}
