use self::{cpu::Cpu, memory::Ram};
use crate::{
    error::MachineError,
    machine::cpu::{execution::execute, instructions::Instruction},
    types::{decompose_address_word, vec_byte_from_hex, word_from_hex, Byte, Word},
};
use error_stack::{Result, ResultExt};
#[allow(unused)]
use log::{debug, trace};
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, fmt::Display, fs::File, io::BufReader, path::Path};

pub mod cpu;
pub mod memory;

pub struct Machine {
    pub cpu: Cpu,
    pub ram: Ram,

    pub current_address: Word,
}

impl Display for Machine {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, " CPU: {}", self.cpu)?;
        // writeln!(f, "   S: {}", self.ram.get_stack())?;
        write!(f, "{}", self.ram)
    }
}

impl Machine {
    pub fn initialized() -> Self {
        Self {
            cpu: Cpu::initialized(),
            ram: Ram::initialized(),
            current_address: 0,
        }
    }

    pub fn from_json<P: AsRef<Path>>(path: P) -> Result<Self, MachineError> {
        let file = File::open(path).change_context_lazy(|| MachineError::FileIoError)?;
        let reader = BufReader::new(file);
        let image_parts: JsonMachineDescription =
            serde_json::from_reader(reader).change_context_lazy(|| MachineError::FileIoError)?;
        trace!("{image_parts:?}");
        let mut current_address = 0;

        let mut cpu = Cpu::initialized();
        let ram;
        if let Ok(ram_map) = image_parts.ram_decode() {
            ram = Ram::from_map(ram_map)?;
            current_address = Self::bytes_to_address(&ram.data[0xfffc..0xfffe]);
            cpu.program_counter.value = current_address;
        } else {
            ram = Ram::initialized();
        }

        Ok(Self {
            cpu,
            ram,
            current_address,
        })
    }

    pub fn step(&mut self) -> Result<(), MachineError> {
        let pc = self.cpu.program_counter.advance();
        self.current_address = pc;

        let opcode = self.ram.data[pc as usize];
        let instruction = Instruction::for_byte(opcode)?;

        execute(self, &instruction)?;
        self.cpu.advance_clock(instruction.cycle_advance);

        Ok(())
    }

    pub fn next_byte(&mut self) -> Result<Byte, MachineError> {
        Ok(self.ram.value_at(self.cpu.program_counter.advance()))
    }

    pub fn next_bytes(&mut self, count: Byte) -> Result<Vec<Byte>, MachineError> {
        (0..count).map(|_| self.next_byte()).collect()
    }

    pub fn next_address(&mut self) -> Result<Word, MachineError> {
        Ok(Self::bytes_to_address(&self.next_bytes(2)?))
    }

    pub fn jump_to_sub(&mut self, address: Word) -> Result<Word, MachineError> {
        let old_location = self.cpu.program_counter.value;
        let mut return_location = decompose_address_word(old_location);
        return_location.reverse();
        for b in return_location {
            self.push_stack(b)?;
        }
        self.cpu.program_counter.value = address;

        Ok(old_location)
    }

    pub fn return_from_sub(&mut self) -> Result<Word, MachineError> {
        let old_location = self.cpu.program_counter.value;
        let address = self.pop_stack_address()?;
        // debug!("Return to: {address:04x}");
        self.cpu.program_counter.value = address;

        Ok(old_location)
    }

    pub fn pop_stack_address(&mut self) -> Result<Word, MachineError> {
        let mut bytes = [0u8; 2];
        #[allow(clippy::needless_range_loop)]
        for i in 0..2 {
            bytes[i] = self.pop_stack()?;
        }
        let address = Self::bytes_to_address(&bytes);
        Ok(address)
    }

    pub fn pop_stack(&mut self) -> Result<Byte, MachineError> {
        self.cpu.stack_pointer.increment();
        let sp = self.cpu.stack_pointer.value;
        let stack_read = sp as usize | 0x0100;
        let value = self.ram.data[stack_read];
        // debug!("Read from stack: {value:02x} ({stack_read:04x})");

        Ok(value)
    }

    pub fn push_stack(&mut self, value: Byte) -> Result<(), MachineError> {
        let sp = self.cpu.stack_pointer.decrement();
        let stack_write = sp as usize | 0x0100;
        // debug!("Push into stack: {value:02x} ({stack_write:04x})");
        self.ram.data[stack_write] = value;

        Ok(())
    }

    fn bytes_to_address(input: &[Byte]) -> Word {
        let lo = input[0] as u32;
        let hi = input[1] as u32;

        ((lo + (hi * 0x100)) & 0xffff) as Word
    }
}

#[derive(Debug, Deserialize, Serialize)]
struct JsonMachineDescription {
    cpu: Option<HashMap<String, String>>,
    ram: Option<HashMap<String, String>>,
}

impl JsonMachineDescription {
    fn ram_decode(&self) -> Result<HashMap<Word, Vec<Byte>>, MachineError> {
        if self.ram.is_none() {
            return Ok(HashMap::default());
        }

        let ram = self.ram.as_ref().unwrap();
        let mut output = HashMap::default();
        for (k, v) in ram {
            let nk = word_from_hex(k.as_str())?;
            let nv = vec_byte_from_hex(v.as_str())?;

            output.insert(nk, nv);
        }

        Ok(output)
    }
}
