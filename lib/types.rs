use crate::error::MachineError;
use error_stack::{Result, ResultExt};
use itertools::Itertools;
// use log::debug;

pub type Bit = bool;
pub type Byte = u8;
pub type Word = u16;

pub fn unsigned_from_hex(input: &str) -> Result<u128, MachineError> {
    u128::from_str_radix(input, 16).change_context_lazy(|| MachineError::BadHexError)
}

pub fn word_from_hex(input: &str) -> Result<Word, MachineError> {
    Ok(unsigned_from_hex(input)? as Word)
}

pub fn byte_from_hex(input: &str) -> Result<Byte, MachineError> {
    Ok(unsigned_from_hex(input)? as Byte)
}

pub fn vec_byte_from_hex(input: &str) -> Result<Vec<Byte>, MachineError> {
    input
        .chars()
        .collect_vec()
        .chunks(2)
        .map(String::from_iter)
        .map(|s| byte_from_hex(&s))
        .collect()
}

pub fn decompose_address_word(input: Word) -> [Byte; 2] {
    let lo = input & 0x00ff;
    let hi = input >> 0x8;
    // debug!("{input:04x} -> ({lo:02x}, {hi:02x})");

    [lo as Byte, hi as Byte]
}
